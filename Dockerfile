
## Stage 1 : build binary
FROM crystallang/crystal:1.4.1 AS builder

COPY . /app
WORKDIR /app
RUN shards build

## Stage 2 : include binary into final image
FROM python:3.7-bullseye AS runner

RUN apt-get update \
 && apt-get -yq install \
 	libevent-core-2.1-7 \
    gettext \
    jq \
    python3-aodhclient \
    python3-barbicanclient \
    python3-ceilometerclient \
    python3-cinderclient \
    python3-cloudkittyclient \
    python3-designateclient \
    python3-gnocchiclient \
    python3-octaviaclient \
    python3-osc-placement \
    python3-openstackclient \
    python3-pankoclient \
    zip \
 && rm -rf /var/lib/apt/lists/*

# RUN wget https://github.com/sapcc/cyclone/releases/download/v0.1.28/cyclone \
#      -O /usr/bin/cyclone \
#  && chmod +x /usr/bin/cyclone

COPY --from=builder /app/bin/arkisto /usr/bin/arkisto

CMD /bin/bash
