

build:
	shards build

docker: docker-build docker-test docker-push

docker-build:
	docker build -t glenux/openstack-arkisto .

docker-push:
	docker push glenux/openstack-arkisto

docker-test:
	docker run glenux/openstack-arkisto arkisto --version
